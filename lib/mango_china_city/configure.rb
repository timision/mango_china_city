module MangoChinaCity
  module Configure

    attr_accessor :redis

    def configure(*options)
      yield self if block_given?
    end

  end
end


