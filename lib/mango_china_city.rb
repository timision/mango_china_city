# frozen_string_literal: true

require_relative "mango_china_city/version"
require_relative "mango_china_city/configure"

module MangoChinaCity

  class << self
    include Configure

    # 国家统计局同步数据
    def sync_data(gen_file=false)
      json = { province: [] }
      base_url = "http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2020/"

      province_res = convert_utf8(
        (RestClient::Request.execute(method: :get, url: "#{base_url}index.html", timeout: 1).body rescue retry)
      )
      province_res.match(/<td>.+<\/td>/).to_s.gsub(/<br\/>|<\/td>|<td>/,'').split("</a>").each do |province|
        # p_url省详情的页面地址， code 省code
        p_url, code = province.match(/(\d+).html/).to_a
        province_ = { name: province.match(/[\u4e00-\u9fa5]+/).to_s, code: "#{code}", city: []}
        puts "正在同步#{province.match(/[\u4e00-\u9fa5]+/).to_s}"
        # 获取省下的市
        city_res = convert_utf8(
          (RestClient::Request.execute(method: :get, url: "#{base_url}#{p_url}", timeout: 1).body rescue retry)
        )

        city_res.gsub(/\r|\n/,'').match(/<a href=.+<\/a>/).to_s.split("</tr>").each do |city|
          c_url, c_code = city.match(/(\d+).html/).to_a
          city_ = { name: city.match(/[\u4e00-\u9fa5]+/).to_s, code: "#{c_code}", district: [] }
          puts "正在同步#{province.match(/[\u4e00-\u9fa5]+/).to_s}::#{city.match(/[\u4e00-\u9fa5]+/).to_s}"
          # 获取市下的区
          district_res = convert_utf8(
            (RestClient::Request.execute(method: :get, url: "#{base_url}#{code}/#{c_url}", timeout: 1).body rescue retry)
          )
          district_res.gsub(/\r|\n/,'').match(/<a href=.+<\/a>/).to_s.split("</tr>").each do |district|
            d_url, d_code = district.match(/(\d+).html/).to_a
            city_[:district] << { name: district.match(/[\u4e00-\u9fa5]+/).to_s, code: d_code }
            puts "正在同步#{province.match(/[\u4e00-\u9fa5]+/).to_s}::#{city.match(/[\u4e00-\u9fa5]+/).to_s}::#{district.match(/[\u4e00-\u9fa5]+/).to_s}"
          end
          province_[:city] << city_
        end

        json[:province] << province_
      end

      # 生成文件
      File.open("#{Rails.root}/db/areas.json", 'w+') do |f|
        f.write(json.to_json)
      end if gen_file
      puts '生在写入数据库...'
      import_data(json)
      puts '写入完成，同步数据结束！'
    end

    # 省市区信息写入数据库
    def import_data(json)
      data_province = Province.all.to_a
      data_city = City.all.to_a
      data_district = District.all.to_a

      Province.transaction do
        json[:province].each do |province|
          province_ = Province.find_or_create_by(name: province[:name], code: province[:code])
          data_province.delete_if{|e| e.code == province_.code}

          province[:city].each do |city|
            city_ = City.find_or_create_by(province: province_, name: city[:name], code: city[:code])
            data_city.delete_if{|e| e.code == city_.code }

            city[:district].each do |district|
              district_ = District.find_or_create_by(province: province_, city: city_, name: district[:name], code: district[:code])
              data_district.delete_if{|e| e.code == district_.code }
            end
          end
        end

        # 软删除更新地区
        [data_province, data_city, data_district].flatten.each{|e| e.destroy}

        # 重建缓存
        build_cache if redis.present?
      end
    end

    # 获取省市区
    def list(code = nil)
      code = code.to_s if code.present?
      if redis.present? && redis.keys.include?("province_cache")
        return JSON.parse( redis.get("province_cache") ).each(&:deep_symbolize_keys!)  if code.nil?
        return JSON.parse( redis.get("city_cache") )[code].each(&:deep_symbolize_keys!) if code.size == 2
        return JSON.parse( redis.get("district_cache") )[code].each(&:deep_symbolize_keys!) if code.size != 2
      else # 数据库查询
        return Province.all.map{|e| {name: e.name, code: e.code}}  if code.nil?
        return City.where(province: Province.find_by(code: code)).map{|e| { name: e.name, code: e.code } } if code.size == 2
        return District.where(city: City.find_by(code: code)).map{|e| { name: e.name, code: e.code }} if code.size != 2
      end
    end

    def get(code)
      code = code.to_s
      result = nil
      if redis.present? && redis.keys.include?("province_cache")
        result = (JSON.parse( redis.get("province_cache") ).find{|e| e["code"] == code}["name"] rescue nil)
        result = (JSON.parse( redis.get("city_cache") ).values.flatten.find{|e| e["code"] == code}["name"] rescue nil) if result.nil?
        result = (JSON.parse( redis.get("district_cache") ).values.flatten.find{|e| e["code"] == code}["name"] rescue nil ) if result.nil?
      else
        result = Province.find_by(code: code).try(:name)
        result = City.find_by(code: code).try(:name) if result.nil?
        result = District.find_by(code: code).try(:name) if result.nil?
      end

      return result.to_s
    end

    # 获取数据库地区并缓存到redis
    def build_cache
      provinces_ = Province.not_deleted.to_a
      cities_ = City.not_deleted.to_a
      districts_ = District.not_deleted.to_a

      cities = {}
      districts = {}
      redis.set("province_cache", provinces_.map{|e| { name: e.name, code: e.code }}.to_json)

      provinces_.each do |province|
        puts "正在缓存\e[0;32;49m<#{province.name}>\e[0m所有数据..."
        cities.merge!( { province.code => cities_.select{|e| e.province_id == province.id}.map{|e| { name: e.name, code: e.code }} } )

        cities_.each do |city|
          districts.merge!( { city.code => districts_.select{|e| e.city_id == city.id }.map{|e| { name: e.name, code: e.code }} } )
        end
        puts "缓存\e[0;32;49m<#{province.name}>\e[0m完成!\n\n"
      end
      redis.set("city_cache", cities.to_json)
      redis.set("district_cache", districts.to_json)
    end

    private
    # 编码格式转换
    def convert_utf8(str)
      str.force_encoding('GB18030').encode('utf-8')
    end

  end
end
