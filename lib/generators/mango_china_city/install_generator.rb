require 'rails/generators'

module MangoChinaCity
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path('../templates', __FILE__)

      def generate_migrates
        copy_file "model/province.rb", "#{Rails.root}/app/models/province.rb"
        copy_file "model/city.rb", "#{Rails.root}/app/models/city.rb"
        copy_file "model/district.rb", "#{Rails.root}/app/models/district.rb"
        copy_file "create_provinces.rb", "#{Rails.root}/db/migrate/#{Time.now.strftime("%Y%m%d%H%m")}01_create_provinces.rb"
        copy_file "create_cities.rb", "#{Rails.root}/db/migrate/#{Time.now.strftime("%Y%m%d%H%m")}02_create_cities.rb"
        copy_file "create_districts.rb", "#{Rails.root}/db/migrate/#{Time.now.strftime("%Y%m%d%H%m")}03_create_districts.rb"
      end
    end
  end
end