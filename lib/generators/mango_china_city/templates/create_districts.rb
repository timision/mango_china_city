class CreateDistricts < ActiveRecord::Migration[6.1]
  def change
    create_table :districts do |t|
      t.references :province
      t.references :city
      t.string :name, index: true, comment: '省名字'
      t.string :code, index: true, comment: '省Code'
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
