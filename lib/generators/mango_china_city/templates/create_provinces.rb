class CreateProvinces < ActiveRecord::Migration[6.1]
  def change
    create_table :provinces do |t|
      t.string :name, index: true, comment: '省名字'
      t.string :code, index: true, comment: '省Code'
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
