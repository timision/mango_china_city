class CreateCities < ActiveRecord::Migration[6.1]
  def change
    create_table :cities do |t|
      t.references :province
      t.string :name, index: true, comment: '省名字'
      t.string :code, index: true, comment: '省Code'
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
