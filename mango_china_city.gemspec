# frozen_string_literal: true

require_relative "lib/mango_china_city/version"

Gem::Specification.new do |spec|
  spec.name = "mango_china_city"
  spec.version = MangoChinaCity::VERSION
  spec.authors = ["刘琦"]
  spec.email = ["l_qi199@163.com"]

  spec.summary = "省、市、区三级数据"
  spec.description = "包含数据迁移、数据同步。"
  spec.homepage = "https://gitee.com/timision/mango_china_city.git"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitee.com/timision/mango_china_city.git"
  spec.metadata["changelog_uri"] = "https://gitee.com/timision/mango_china_city.git"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  # spec.add_development_dependency "rspec"
  spec.add_development_dependency "rails", "~> 6.1.0"
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
